﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodDiaryWeb2.Migrations
{
    public partial class AllModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "PortionId",
                table: "Eating_Product",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "EatingId",
                table: "Eating_Product",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateTable(
                name: "Eating_Type",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Eating_Type", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Portion",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(unicode: false, nullable: true),
                    Weight = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Portion", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Target",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(unicode: false, nullable: true),
                    Calories = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Target", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Client",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Weight = table.Column<double>(nullable: false),
                    Height = table.Column<double>(nullable: false),
                    Name = table.Column<string>(unicode: false, nullable: true),
                    TargetId = table.Column<int>(nullable: true),
                    Age = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Client", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Client_Target_TargetId",
                        column: x => x.TargetId,
                        principalTable: "Target",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Eating",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Eating_TypeId = table.Column<int>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    ClientId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Eating", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Eating_Client_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Client",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Eating_Eating_Type_Eating_TypeId",
                        column: x => x.Eating_TypeId,
                        principalTable: "Eating_Type",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Eating_Product_EatingId",
                table: "Eating_Product",
                column: "EatingId");

            migrationBuilder.CreateIndex(
                name: "IX_Eating_Product_PortionId",
                table: "Eating_Product",
                column: "PortionId");

            migrationBuilder.CreateIndex(
                name: "IX_Client_TargetId",
                table: "Client",
                column: "TargetId");

            migrationBuilder.CreateIndex(
                name: "IX_Eating_ClientId",
                table: "Eating",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Eating_Eating_TypeId",
                table: "Eating",
                column: "Eating_TypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Eating_Product_Eating_EatingId",
                table: "Eating_Product",
                column: "EatingId",
                principalTable: "Eating",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Eating_Product_Portion_PortionId",
                table: "Eating_Product",
                column: "PortionId",
                principalTable: "Portion",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Eating_Product_Eating_EatingId",
                table: "Eating_Product");

            migrationBuilder.DropForeignKey(
                name: "FK_Eating_Product_Portion_PortionId",
                table: "Eating_Product");

            migrationBuilder.DropTable(
                name: "Eating");

            migrationBuilder.DropTable(
                name: "Portion");

            migrationBuilder.DropTable(
                name: "Client");

            migrationBuilder.DropTable(
                name: "Eating_Type");

            migrationBuilder.DropTable(
                name: "Target");

            migrationBuilder.DropIndex(
                name: "IX_Eating_Product_EatingId",
                table: "Eating_Product");

            migrationBuilder.DropIndex(
                name: "IX_Eating_Product_PortionId",
                table: "Eating_Product");

            migrationBuilder.AlterColumn<int>(
                name: "PortionId",
                table: "Eating_Product",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "EatingId",
                table: "Eating_Product",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
