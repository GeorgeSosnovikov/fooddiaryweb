﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodDiaryWeb2.Migrations
{
    public partial class Second : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Eating_FK",
                table: "Eating_Product");

            migrationBuilder.RenameColumn(
                name: "Product_FK",
                table: "Eating_Product",
                newName: "PortionId");

            migrationBuilder.RenameColumn(
                name: "Portion_FK",
                table: "Eating_Product",
                newName: "EatingId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PortionId",
                table: "Eating_Product",
                newName: "Product_FK");

            migrationBuilder.RenameColumn(
                name: "EatingId",
                table: "Eating_Product",
                newName: "Portion_FK");

            migrationBuilder.AddColumn<int>(
                name: "Eating_FK",
                table: "Eating_Product",
                nullable: false,
                defaultValue: 0);
        }
    }
}
