﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodDiaryWeb2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FoodDiaryWeb2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EatingController : ControllerBase
    {
        private readonly DataContext _context;
        public EatingController(DataContext context)
        {
            _context = context;
        }
        //получить все приёмы пищи
        [HttpGet]
        public IEnumerable<Eating> GetAll()
        {
            return _context.Eating;
        }
        //получить последний созданный приём пищи
        [HttpGet("last")]
        public Eating GetLast()
        {
            var eating = _context.Eating.Last();
            return eating;
        }
        //получить приёмы пищи за сегодня
        [HttpGet("today/{type}/{currentUser}")]
        public Eating GetEatingToday([FromRoute] int type, [FromRoute] string currentUser)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}
            var client = _context.Client.Where(p => p.Name == currentUser);
            Client b = null;
            if (client.Count() != 0)
                b = client.First();
            var eating = _context.Eating.Include(p => p.Eating_Type).Include(p => p.Client)
                        .Where(m => (m.Eating_TypeId == type) && (m.Time.Day == DateTime.Now.Day)
                        && (m.Time.Month == DateTime.Now.Month) && (m.Time.Year == DateTime.Now.Year) );
            Eating a = null;
            if (eating.Count() != 0)
                a = eating.First();
            if ((a == null) && (b != null))
            {
                var e = new Eating { Eating_TypeId = type, Time = DateTime.Now, ClientId = b.Id };
                _context.Eating.Add(e);
                _context.SaveChanges();
                //return NotFound();
                return e;
            }
            else
                return a;
        }
        //получить конкретный приём пищи
        [HttpGet("{id}")]
        public async Task<IActionResult> GetEating([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var eating = await _context.Eating.Include(p => p.Eating_Type).Include(p => p.Client).SingleOrDefaultAsync(m => m.Id == id);
            if (eating == null)
            {
                return NotFound();
            }
            return Ok(eating);
        }
        //создать приём пищи
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Eating eating)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Eating.Add(eating);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEating", new { id = eating.Id }, eating);
        }
    }
}