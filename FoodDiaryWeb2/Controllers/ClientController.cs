﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodDiaryWeb2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FoodDiaryWeb2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private readonly DataContext _context;
        public ClientController(DataContext context)
        {
            _context = context;
        }
        //получить всех клиентов
        [HttpGet]
        public IEnumerable<Client> GetAll()
        {
            return _context.Client.Include(m => m.TargetId);
        }
        //получить клиента по имени
        [HttpGet("{currentUser}")]
        public async Task<IActionResult> GetClient([FromRoute] string currentUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var client = await _context.Client.SingleOrDefaultAsync(m => m.Name == currentUser);
            if (client == null)
            {
                return NotFound();
            }
            return Ok(client);
        }
        //добавить пользователя
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Client client)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Client.Add(client);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetClient", new { id = client.Id }, client);
        }
        //изменить данные о пользователе
        [HttpPut("{currentUser}")]
        public async Task<IActionResult> Update([FromRoute] string currentUser, [FromBody] Client client)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var item = _context.Client.Where(m => m.Name == currentUser);
            Client a = null;
            if (item.Count() != 0)
                a = item.First();
            else
                return NotFound();
            //if (item == null)
            //{
            //    return NotFound();
            //}
            a.Height = client.Height;
            a.Name = client.Name;
            a.Age = client.Age;
            a.TargetId = client.TargetId + 1;
            a.Weight = client.Weight;

            _context.Client.Update(a);
            await _context.SaveChangesAsync();
            return NoContent();
        }
        //удалить пользователя
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var item = _context.Client.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            _context.Client.Remove(item);
            await _context.SaveChangesAsync();
            return NoContent();
        }
    }
}