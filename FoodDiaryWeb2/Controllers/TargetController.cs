﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodDiaryWeb2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FoodDiaryWeb2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TargetController : ControllerBase
    {
        private readonly DataContext _context;
        public TargetController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Target> GetAll()
        {
            return _context.Target;
        }
    }
}