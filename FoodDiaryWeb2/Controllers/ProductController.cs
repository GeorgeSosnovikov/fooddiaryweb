﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodDiaryWeb2.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FoodDiaryWeb2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly DataContext _context;
        public ProductController(DataContext context)
        {
            _context = context;
        }
        //получить все продукты
        [HttpGet]
        public IEnumerable<Product> GetAll()
        {
            return _context.Product;
        }
        //получить конкретный продукт
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProduct([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var product = await _context.Product.SingleOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }
        //создать продукт
        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Product.Add(product);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProduct", new { id = product.Id }, product);
        }
        //изменить инфу про продукт
        [HttpPut("{id}")]
        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var item = _context.Product.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            item.Calories = product.Calories;
            item.Carbohydrate = product.Carbohydrate;
            item.Fat = product.Fat;
            item.Image = product.Image;
            item.Name = product.Name;
            item.Protein = product.Protein;
       
            _context.Product.Update(item);
            await _context.SaveChangesAsync();
            return NoContent();
        }
        //удалить продукт
        [Authorize(Roles = "admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var item = _context.Product.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            var items = _context.Eating_Product.Where(e => e.ProductId == id);
            if (items.Count() != 0)
                foreach (var a in items)
                {
                    _context.Eating_Product.Remove(a);
                }
            _context.Product.Remove(item);
            await _context.SaveChangesAsync();
            return NoContent();
        }
    }
}