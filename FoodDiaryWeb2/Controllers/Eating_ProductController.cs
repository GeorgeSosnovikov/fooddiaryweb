﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodDiaryWeb2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FoodDiaryWeb2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Eating_ProductController : ControllerBase
    {
        private readonly DataContext _context;
        public Eating_ProductController(DataContext context)
        {
            _context = context;
        }
        //получить съеденные продукты за сегодня
        [HttpGet("curr/{currentUser}")]
        public IEnumerable<Eating_Product> GetToday([FromRoute] string currentUser)
        {
            

            var a = _context.Eating_Product.Include(p => p.Product).Include(p => p.Portion).Include(e => e.Eating)
                .Where(e => e.Eating.Time.Day == DateTime.Now.Day
                && e.Eating.Time.Month == DateTime.Now.Month 
                && e.Eating.Time.Year == DateTime.Now.Year)
                .Where(f => f.Eating.Client.Name == currentUser);
            return a;
        }
        //общее количество съеденных калорий за сегодня
        [HttpGet("todayCalories/{currentUser}")]
        public double GetTodayCalories([FromRoute] string currentUser)
        {
            double sum = 0;
            var res1 = (from d in _context.Eating_Product
                        join pr in _context.Product on d.ProductId equals pr.Id
                        join port in _context.Portion on d.PortionId equals port.Id
                        join e in _context.Eating on d.EatingId equals e.Id
                        where (e.Time.Day == DateTime.Now.Day
                                && e.Time.Month == DateTime.Now.Month
                                && e.Time.Year == DateTime.Now.Year && e.Client.Name == currentUser)
                        select (port.Weight / 100 * pr.Calories)).ToList();
            foreach (double p in res1)
                sum += p;
            return Math.Round(sum, 2);
        }
        //получить съеденный продукт
        [HttpGet("{id}")]
        public async Task<IActionResult> GetEating_Product([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var eating_product = await _context.Eating_Product.Include(p => p.Product).Include(p => p.Portion).Include(e => e.Eating).SingleOrDefaultAsync(m => m.Id == id);
            if (eating_product == null)
            {
                return NotFound();
            }
            return Ok(eating_product);
        }
        //создать съеденный продукт
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Eating_Product eating_product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Eating_Product.Add(eating_product);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEating_Product", new { id = eating_product.Id }, eating_product);
        }
        //изменить съеденный продукт
        [HttpPut("{id}")]
        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] Eating_Product eating_product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var item = _context.Eating_Product.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            item.EatingId = eating_product.EatingId;
            item.ProductId = eating_product.ProductId;
            item.PortionId = eating_product.PortionId;
            _context.Eating_Product.Update(item);
            await _context.SaveChangesAsync();
            return NoContent();
        }
        //удалить съеденный продукт
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var item = _context.Eating_Product.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            _context.Eating_Product.Remove(item);
            await _context.SaveChangesAsync();
            return NoContent();
        }
        //удалить несколько
        [HttpDelete("some/{id}")]
        public async Task<IActionResult> DeleteSome([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var items = _context.Eating_Product.Where(e => e.ProductId == id);
            if (items.Count() == 0)
                return NotFound();
            foreach (var a in items)
            {
                _context.Eating_Product.Remove(a);
            }
            await _context.SaveChangesAsync();
            //var item = _context.Eating_Product.Find(id);
            //if (item == null)
            //{
            //    return NotFound();
            //}
            //_context.Eating_Product.Remove(item);
            //await _context.SaveChangesAsync();
            return NoContent();
        }
    }
}