﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodDiaryWeb2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FoodDiaryWeb2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PortionController : ControllerBase
    {
        private readonly DataContext _context;
        public PortionController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Portion> GetAll()
        {
            return _context.Portion;
        }
    }
}