﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodDiaryWeb2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FoodDiaryWeb2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Eating_TypeController : ControllerBase
    {
        private readonly DataContext _context;
        public Eating_TypeController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Eating_Type> GetAll()
        {
            return _context.Eating_Type;
        }
    }
}