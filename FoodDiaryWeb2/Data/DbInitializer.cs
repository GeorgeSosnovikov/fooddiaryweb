﻿using FoodDiaryWeb2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodDiaryWeb2.Data
{
    public class DbInitializer
    {
        public static void Initialize(DataContext context)
        {
            context.Database.EnsureCreated();

            if (context.Product.Any())
            {
                return;
            }
            if (context.Product.Count() != 0)
            {

            }
            else
            {
                var products = new Product[]
                {
                new Product{Name="Творог 5%", Calories=150, Carbohydrate=0.5, Fat=5, Protein=16, Image = new byte[0]},
                new Product{Name="Молоко 3.2%", Calories=100, Carbohydrate=3, Fat=3.2, Protein=4, Image = new byte[0]},
                new Product{Name="Сметана 20%", Calories=300, Carbohydrate=4, Fat=20, Protein=4, Image = new byte[0]}

                };
                foreach (Product p in products)
                {
                    context.Product.Add(p);
                }
                context.SaveChanges();
            }
            if (context.Portion.Count() != 0)
            {
                
            }
            else
            {
                var portions = new Portion[]
                {
                    new Portion { Name="100 грамм", Weight=100 },
                    new Portion { Name="200 грамм", Weight=200 },
                    new Portion { Name="300 грамм", Weight=300 }
                };
                foreach (Portion p in portions)
                {
                    context.Portion.Add(p);
                }
                context.SaveChanges();
            }
            if (context.Target.Count() != 0)
            {
                
            }
            else
            {
                var targets = new Target[]
                {
                    new Target { Name="Набор мышечной массы" },
                    new Target { Name="Похудение" },
                    new Target { Name="Сохранение веса" }
                };
                foreach (Target p in targets)
                {
                    context.Target.Add(p);
                }
                context.SaveChanges();
            }
            if (context.Client.Count() != 0)
            {
                
            }
            else
            {
                var clients = new Client[]
                {
                    new Client { Weight=186, Height=85, Age=20, Name="George", TargetId=1 },
                    new Client { Weight=170, Height=65, Age=17, Name="Vasek", TargetId=3 },
                    new Client { Weight=181, Height=115, Age=35, Name="Petr", TargetId=2 }
                };
                foreach (Client p in clients)
                {
                    context.Client.Add(p);
                }
                context.SaveChanges();
            }
            if (context.Eating_Type.Count() != 0)
            {

            }
            else
            {
                var types = new Eating_Type[]
                {
                    new Eating_Type { Name="Breakfast" },
                    new Eating_Type { Name="Lunch" },
                    new Eating_Type { Name="Dinner" }
                };
                foreach (Eating_Type p in types)
                {
                    context.Eating_Type.Add(p);
                }
                context.SaveChanges();
            }
            if (context.Eating.Count() != 0)
            {

            }
            else
            {
            var eatings = new Eating[]
                {
                    new Eating { Eating_TypeId=1, Time = DateTime.Now, ClientId=1 },
                    new Eating { Eating_TypeId=2, Time = DateTime.Now, ClientId=1 },
                    new Eating { Eating_TypeId=3, Time = DateTime.Now, ClientId=1 }
                };
                foreach (Eating p in eatings)
                {
                    context.Eating.Add(p);
                }
                context.SaveChanges();
            }
            if (context.Eating_Product.Count() != 0)
            {

            }
            else
            {
                var eating_products = new Eating_Product[]
                {
                    new Eating_Product { ProductId=4, PortionId=1, EatingId=1 },
                    new Eating_Product { ProductId=5, PortionId=2, EatingId=2 },
                    new Eating_Product { ProductId=6, PortionId=3, EatingId=3 }
                };
                foreach (Eating_Product p in eating_products)
                {
                    context.Eating_Product.Add(p);
                }
                context.SaveChanges();
            }
            
        }
    }
}
