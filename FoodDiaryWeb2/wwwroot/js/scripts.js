﻿/*jshint esversion: 6 */
//let currentUser = "";

//получение съеденных продуктов за сегодня
var Eating_Product = (function () {

    var init = function () {
        getData();
    };

    var getData = function () {
        let currentUser = CurrentUser.init();
        let items = null;
        var i, j, x = "";
        var y = "", z = "";
        var request = new XMLHttpRequest();
        request.open("GET", "/api/Eating_Product/curr/" + currentUser, false);
        request.onload = function () {
            items = JSON.parse(request.responseText);
            for (i in items) {

                if (items[i].eating.eating_TypeId == 1) {
                    x += "<hr>";
                    x += "<dl class=&quot;dl - horizontal&quot;> <dt>" + items[i].product.name + ", " + items[i].portion.weight + " грамм" +
                        "</dt> <dd>" + "Углеводы: " + items[i].product.carbohydrate * items[i].portion.weight / 100 +
                        ", Белки: " + items[i].product.protein * items[i].portion.weight / 100 +
                        ", Жиры: " + items[i].product.fat * items[i].portion.weight / 100 +
                        " Калорий: " + items[i].product.calories * items[i].portion.weight / 100 +
                        "</dd> </dl>";
                    x += "<button type='button' class='btn btn-danger' onclick='deleteEating_Product(" + items[i].id + ");'>Удалить</button>";
                }
                if (items[i].eating.eating_TypeId == 2) {
                    y += "<hr>";
                    y += "<dl class=&quot;dl - horizontal&quot;> <dt>" + items[i].product.name + ", " + items[i].portion.weight + " грамм" +
                        "</dt> <dd>" + "Углеводы: " + items[i].product.carbohydrate * items[i].portion.weight / 100 +
                        ", Белки: " + items[i].product.protein * items[i].portion.weight / 100 +
                        ", Жиры: " + items[i].product.fat * items[i].portion.weight / 100 +
                        " Калорий: " + items[i].product.calories * items[i].portion.weight / 100 +
                        "</dd> </dl>";
                    y += "<button type='button' class='btn btn-danger' onclick='deleteEating_Product(" + items[i].id + ");'>Удалить</button>";
                }
                if (items[i].eating.eating_TypeId == 3) {
                    z += "<hr>";
                    z += "<dl class=&quot;dl - horizontal&quot;> <dt>" + items[i].product.name + ", " + items[i].portion.weight + " грамм" +
                        "</dt> <dd>" + "Углеводы: " + items[i].product.carbohydrate * items[i].portion.weight / 100 +
                        ", Белки: " + items[i].product.protein * items[i].portion.weight / 100 +
                        ", Жиры: " + items[i].product.fat * items[i].portion.weight / 100 +
                        " Калорий: " + items[i].product.calories * items[i].portion.weight / 100 +
                        "</dd> </dl>";
                    z += "<button type='button' class='btn btn-danger' onclick='deleteEating_Product(" + items[i].id + ");'>Удалить</button>";
                }
            }
            document.getElementById("breakfast").innerHTML = x;
            document.getElementById("lunch").innerHTML = y;
            document.getElementById("dinner").innerHTML = z;
        };
        request.send();
        //подсчёт калорий
        var calories = "";
        items = null;
        var request2 = new XMLHttpRequest();
        request2.open("GET", "/api/Eating_Product/todayCalories/" + currentUser, false);
        request2.onload = function () {
            items = request2.responseText;
            for (i in items) {
                calories += items[i];
            }
            document.getElementById("todayCalories").innerHTML = "Сегодня " + calories + " калорий";
        };
        request2.send();
    };
    return {
        init: init
    };
})();
//текущий пользователь
var CurrentUser = (function () {
    let user = "";

    var init = function () {
        get();
        return user;
    };

    var get = function () {
        let request = new XMLHttpRequest();
        request.open("POST", "/api/Account/isAuthenticated", false);
        request.onload = function () {
            let myObj = "";
            if (request.responseText !== "") {
                myObj = JSON.parse(request.responseText);
                user = myObj.message;
            }
            else
                user = "Гость";
            document.getElementById("msg").innerHTML = user;
        };
        request.send();
        //user = "user@mail.com";
    };

    return {
        init: init
    };
})();
//получения списка продуктов
var Product = (function () {

    var init = function () {
        getData();
    };

    var getData = function () {
        var i = "";
        let items = null;
        var products = "";
        var request2 = new XMLHttpRequest();
        request2.open("GET", "/api/Product/", false);
        request2.onload = function () {
            items = JSON.parse(request2.responseText);
            for (i in items) {
                products += "<option value='" + items[i].id + "'>" + items[i].name + "</option>";
            }
            document.getElementById("products").innerHTML = products;
            document.getElementById("products2").innerHTML = products;
        };
        request2.send();
    };
    return {
        init: init
    };
})();
//получение списка порций
var Portion = (function () {

    var init = function () {
        getData();
    };

    var getData = function () {
        var i = "";
        let items = null;
        var portions = "";
        var request3 = new XMLHttpRequest();
        request3.open("GET", "/api/Portion/", false);
        request3.onload = function () {
            items = JSON.parse(request3.responseText);
            for (i in items) {
                portions += "<option value='" + items[i].id + "'>" + items[i].weight + " грамм</option>";
            }
            document.getElementById("portions").innerHTML = portions;
        };
        request3.send();
    };

    return {
        init: init
    };
})();
//получение типов приёма пищи
var Eating_Type = (function () {

    var init = function () {
        getData();
    };

    var getData = function () {
        var i = "";
        let items = null;
        var typeName = "";
        var eating_types = "";
        var request4 = new XMLHttpRequest();
        request4.open("GET", "/api/Eating_Type/", false);
        request4.onload = function () {
            items = JSON.parse(request4.responseText);
            for (i in items) {
                if (items[i].id == 1)
                    typeName = "Завтрак";
                if (items[i].id == 2)
                    typeName = "Обед";
                if (items[i].id == 3)
                    typeName = "Ужин";
                eating_types += "<option value='" + items[i].id + "'>" + typeName + "</option>";
            }
            document.getElementById("eating_types").innerHTML = eating_types;
        };
        request4.send();
    };

    return {
        init: init
    };
})();
//получение целей
var Target = (function () {

    var init = function () {
        getData();
    };

    var getData = function () {
        var i = "";
        let items = null;
        var typeName = "";
        var targets = "";
        var request10 = new XMLHttpRequest();
        request10.open("GET", "/api/Target/", false);
        request10.onload = function () {
            items = JSON.parse(request10.responseText);
            for (i in items) {
                if (items[i].id == 1)
                    typeName = "Набор мышечной массы";
                if (items[i].id == 2)
                    typeName = "Похудение";
                if (items[i].id == 3)
                    typeName = "Поддержание веса";
                targets += "<option value='" + items[i].id + "'>" + typeName + "</option>";
            }
            document.getElementById("clienttarget").innerHTML = targets;
        };
        request10.send();
    };

    return {
        init: init
    };
})();
//получение данных о пользователе
var Client = (function () {

    var init = function () {
        getData();
    };

    var getData = function () {
        var currentUser = "";
        currentUser = CurrentUser.init();
        if (currentUser == "Гость"){
            document.getElementById("clientage").value = "";
            document.getElementById("clientweight").value = "";
            document.getElementById("clientheight").value = "";
            document.getElementById("clienttarget").options.selectedIndex = "";
        }
        var a = "";
        var client_id = 1;
        var request9 = new XMLHttpRequest();
        request9.open("GET", "/api/Client/" + currentUser, false);
        request9.onload = function () {
            a = JSON.parse(request9.responseText);
            client_id = a.id;
            document.getElementById("clientage").value = a.age;
            document.getElementById("clientweight").value = a.weight;
            document.getElementById("clientheight").value = a.height;
            document.getElementById("clienttarget").options.selectedIndex = a.targetid;
        };
        request9.send();
    };

    return {
        init: init
    };
})();
//изменить данные пользователя
function updateClient() {
    var clientWeight = document.getElementById("clientweight").value;
    var clientHeight = document.getElementById("clientheight").value;
    var clientName = CurrentUser.init();
    var clientTarget = document.getElementById("clienttarget").options.selectedIndex;
    var clientAge = document.getElementById("clientage").value;
    var request11 = new XMLHttpRequest();
    request11.open("PUT", "/api/Client/" + clientName, false);
    request11.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request11.onload = function () {
        Client.init();
        //loadEating_Product();
    };
    request11.send(JSON.stringify({ weight: clientWeight, height: clientHeight, name: clientName, targetid: clientTarget, age: clientAge }));
}
//удалить продукт из съеденных
function deleteEating_Product(id) {
    var request8 = new XMLHttpRequest();
    var url = "/api/Eating_Product/" + id;
    request8.open("DELETE", url, false);
    request8.onload = function () {
        Eating_Product.init();
        //loadEating_Product();
    };
    request8.send();
}
//создать приём пищи
function createEating() {
    var eating_type = document.getElementById("eating_types").value;
    var i = "";
    var lastEatingId;
    var a = "";
    var request6 = new XMLHttpRequest();
    request6.open("GET", "/api/Eating/today/" + eating_type + "/" + CurrentUser.init(), false);
    request6.onload = function () {
        a = JSON.parse(request6.responseText);
        lastEatingId = a.id;
    };
    request6.send();

    var product = document.getElementById("products").value;
    var portion = document.getElementById("portions").value;
    var request7 = new XMLHttpRequest();
    request7.open("POST", "/api/Eating_Product/", false);
    request7.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request7.onload = function () {
        Eating_Product.init();
        //loadEating_Product();
    };
    request7.send(JSON.stringify({ portionid: portion, eatingid: lastEatingId, productid: product }));
}
//создать продукт
function createProduct() {
    var name = document.getElementById("productname").value;
    var calories = document.getElementById("productcalories").value;
    var fat = document.getElementById("productfat").value;
    var protein = document.getElementById("productprotein").value;
    var carbohydrate = document.getElementById("productcarbohydrate").value;
    var request7 = new XMLHttpRequest();
    request7.open("POST", "/api/Product/", false);
    request7.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request7.onload = function () {
        var msg = "";
        if (request7.status === 401) {
            msg = "У вас не хватает прав для создания";
        } else if (request7.status === 201) {
            Product.init();
            //loadEating_Product();
        } else {
            msg = "Неизвестная ошибка";
        }
        document.querySelector("#actionMsg").innerHTML = msg;
    };
    request7.send(JSON.stringify({ name: name, calories: calories, fat: fat, protein: protein, carbohydrate: carbohydrate }));
}
//удалить продукт
function deleteProduct() {
    var product = document.getElementById("products2").value;
    var request8 = new XMLHttpRequest();
    request8.open("DELETE", "api/Product/" + product, false);
    request8.onload = function () {
        var msg = "";
        if (request8.status === 401) {
            msg = "У вас не хватает прав для удаления";
        } else if (request8.status === 204) {
            Product.init();
            //loadEating_Product();
        } else {
            msg = "Неизвестная ошибка";
        }
        document.querySelector("#actionMsg").innerHTML = msg;
    };
    request8.send();
}
//войти в систему
function logIn() {
    var email, password = "";
    // Считывание данных с формы
    email = document.getElementById("Email").value;
    password = document.getElementById("Password").value;
    var request = new XMLHttpRequest();
    request.open("POST", "/api/Account/Login", false);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.onreadystatechange = function () {
        // Очистка контейнера вывода сообщений
        document.getElementById("msg").innerHTML = "";
        var mydiv = document.getElementById('formError');
        while (mydiv.firstChild) {
            mydiv.removeChild(mydiv.firstChild);
        }
        // Обработка ответа от сервера
        if (request.responseText !== "") {
            var msg = null;
            msg = JSON.parse(request.responseText);
            document.getElementById("msg").innerHTML = msg.message;
            // Вывод сообщений об ошибках
            if (typeof msg.error !== "undefined" && msg.error.length > 0) {
                for (var i = 0; i < msg.error.length; i++) {
                    var ul = document.getElementsByTagName("ul");
                    var li = document.createElement("li");
                    li.appendChild(document.createTextNode(msg.error[i]));
                    ul[0].appendChild(li);
                }
            }
            else {
                App.init()
                //loadEating_Product();
            }
            document.getElementById("Password").value = "";
        }
    };
    // Запрос на сервер
    request.send(JSON.stringify({
        email: email,
        password: password
    }));

    App.Init();
}
//выйти из системы
function logOff() {
    var request = new XMLHttpRequest();
    request.open("POST", "api/account/logoff", false);
    request.onload = function () {
        var msg = JSON.parse(this.responseText);
        document.getElementById("msg").innerHTML = "";
        var mydiv = document.getElementById('formError');
        while (mydiv.firstChild) {
            mydiv.removeChild(mydiv.firstChild);
        }
        document.getElementById("msg").innerHTML = msg.message;
        
    };
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send();
    App.init();
}

var App = (function () {
    //Объект, содержащий публичное API
    return {
        init: function () {
            // Инициализация модуля. В ней мы инициализируем все остальные модули на странице
            CurrentUser.init();
            Eating_Product.init();
            Product.init();
            Portion.init();
            Eating_Type.init();
            Target.init();
            Client.init();
        }
    };
})();

document.addEventListener("DOMContentLoaded", function (event) {

    document.getElementById("loginBtn").addEventListener("click", logIn);
    document.getElementById("logoffBtn").addEventListener("click", logOff);
    App.init();
});


