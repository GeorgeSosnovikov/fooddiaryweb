﻿const uri = "/api/account/Register";
var a = true;
function Register() {
    // Считывание данных с формы
    a = true;
    var email = document.querySelector("#email").value;
    var password = document.querySelector("#password").value;
    var passwordConfirm = document.querySelector("#passwordConfirm").value;

    let request = new XMLHttpRequest();
    request.open("POST", uri, false);
    request.setRequestHeader("Accepts", "application/json;charset=UTF-8");
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    // Обработка ответа
    request.onload = function () {
        ParseResponse(this);
    };
    // Запрос на сервер
    request.send(JSON.stringify({
        email: email,
        password: password,
        passwordConfirm: passwordConfirm
    }));

    //Создание клиента
    if (a) {
        var name = document.querySelector("#email").value;
        var weight = 60;
        var height = 170;
        var age = 18;
        var target = 2;
        var request2 = new XMLHttpRequest();
        request2.open("POST", "/api/Client/", false);
        request2.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        request2.onload = function () {
        };
        request2.send(JSON.stringify({ weight: weight, height: height, name: name, targetid: target, age: age }));
    }
}

// Разбор ответа
function ParseResponse(e) {
    // Очистка контейнера вывода сообщений
    document.querySelector("#msg").innerHTML = "";
    var formError = document.querySelector("#formError");
    while (formError.firstChild) {
        formError.removeChild(formError.firstChild);
    }
    // Обработка ответа от сервера
    let response = JSON.parse(e.responseText);
    document.querySelector("#msg").innerHTML = response.message;
    // Вывод сообщений об ошибках
    if (response.error.length > 0) {
        a = false;
        for (var i = 0; i < response.error.length; i++) {
            let ul = document.querySelector("ul");
            let li = document.createElement("li");
            li.appendChild(document.createTextNode(response.error[i]));
            ul.appendChild(li);
        }
    }
    else {
        
    }
    // Очистка полей паролей
    document.querySelector("#password").value = "";
    document.querySelector("#passwordConfirm").value = "";
}

// Обработка клика по кнопке регистрации
document.querySelector("#registerBtn").addEventListener("click", Register);