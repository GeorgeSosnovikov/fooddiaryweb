﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodDiaryWeb2.Models
{
    public class DataContext : IdentityDbContext<User>
    {
        #region Constructor
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        { }
        #endregion

        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<Eating> Eating { get; set; }
        public virtual DbSet<Eating_Product> Eating_Product { get; set; }
        public virtual DbSet<Eating_Type> Eating_Type { get; set; }
        public virtual DbSet<Portion> Portion { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<Target> Target { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.Name)
                      .IsUnicode(false);
            });

            modelBuilder.Entity<Eating_Product>(entity =>
            {
                entity.HasOne(d => d.Eating)
                .WithMany(p => p.Eating_Product)
                .HasForeignKey(d => d.EatingId);
            });

            modelBuilder.Entity<Eating_Product>(entity =>
            {
                entity.HasOne(d => d.Portion)
                .WithMany(p => p.Eating_Product)
                .HasForeignKey(d => d.PortionId);
            });

            modelBuilder.Entity<Eating_Product>(entity =>
            {
                entity.HasOne(d => d.Product)
                .WithMany(p => p.Eating_Product)
                .HasForeignKey(d => d.ProductId);
            });

            modelBuilder.Entity<Eating>(entity =>
            {
                entity.HasOne(d => d.Client)
                .WithMany(p => p.Eating)
                .HasForeignKey(d => d.ClientId);
            });

            modelBuilder.Entity<Eating>(entity =>
            {
                entity.HasOne(d => d.Eating_Type)
                .WithMany(p => p.Eating)
                .HasForeignKey(d => d.Eating_TypeId);
            });

            modelBuilder.Entity<Client>(entity =>
            {
                entity.HasOne(d => d.Target)
                .WithMany(p => p.Client)
                .HasForeignKey(d => d.TargetId);
            });
            //modelBuilder.Entity<Client>(entity =>
            //{
            //    entity.Property(e => e.Name)
            //          .IsUnicode(false);
            //});

            //modelBuilder.Entity<Eating_Type>(entity =>
            //{
            //    entity.Property(e => e.Name)
            //          .IsUnicode(false);
            //});

            //modelBuilder.Entity<Portion>(entity =>
            //{
            //    entity.Property(e => e.Name)
            //          .IsUnicode(false);
            //});

            //modelBuilder.Entity<Target>(entity =>
            //{
            //    entity.Property(e => e.Name)
            //          .IsUnicode(false);
            //});

        }
    }
}
