﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodDiaryWeb2.Models
{
    public class Eating
    {
        public Eating()
        {
            Eating_Product = new HashSet<Eating_Product>();
        }

        public int Id { get; set; }

        public int? Eating_TypeId { get; set; }

        public DateTime Time { get; set; }

        public int? ClientId { get; set; }

        public virtual Client Client { get; set; }

        public virtual Eating_Type Eating_Type { get; set; }

        public virtual ICollection<Eating_Product> Eating_Product { get; set; }
    }
}
