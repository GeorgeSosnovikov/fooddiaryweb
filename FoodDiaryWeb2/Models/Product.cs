﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodDiaryWeb2.Models
{
    public class Product
    {
        public Product()
        {
            Eating_Product = new HashSet<Eating_Product>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public byte[] Image { get; set; }

        public double Calories { get; set; }

        public double Fat { get; set; }

        public double Protein { get; set; }

        public double Carbohydrate { get; set; }

        public virtual ICollection<Eating_Product> Eating_Product { get; set; }
    }
}
