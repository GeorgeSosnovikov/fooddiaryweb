﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodDiaryWeb2.Models
{
    public class Client
    {
        public Client()
        {
            Eating = new HashSet<Eating>();
        }

        public int Id { get; set; }

        public double Weight { get; set; }

        public double Height { get; set; }

        public string Name { get; set; }

        public int? TargetId { get; set; }

        public int Age { get; set; }

        public virtual Target Target { get; set; }

        public virtual ICollection<Eating> Eating { get; set; }
    }
}
