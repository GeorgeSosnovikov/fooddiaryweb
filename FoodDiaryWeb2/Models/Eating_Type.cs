﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodDiaryWeb2.Models
{
    public class Eating_Type
    {
        public Eating_Type()
        {
            Eating = new HashSet<Eating>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Eating> Eating { get; set; }
    }
}
