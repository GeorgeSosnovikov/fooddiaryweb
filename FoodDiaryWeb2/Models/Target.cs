﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodDiaryWeb2.Models
{
    public class Target
    {
        public Target()
        {
            Client = new HashSet<Client>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public double? Calories { get; set; }

        public virtual ICollection<Client> Client { get; set; }
    }
}
