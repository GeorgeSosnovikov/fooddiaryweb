﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodDiaryWeb2.Models
{
    public class Eating_Product
    {
        public int? EatingId { get; set; }

        public int? ProductId { get; set; }

        public int Id { get; set; }

        public int? PortionId { get; set; }

        public virtual Eating Eating { get; set; }

        public virtual Portion Portion { get; set; }

        public virtual Product Product { get; set; }
    }
}
