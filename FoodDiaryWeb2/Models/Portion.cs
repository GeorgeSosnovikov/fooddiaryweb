﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodDiaryWeb2.Models
{
    public class Portion
    {
        public Portion()
        {
            Eating_Product = new HashSet<Eating_Product>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public double Weight { get; set; }

        public virtual ICollection<Eating_Product> Eating_Product { get; set; }
    }
}
